#  Copyright (C) 2002-2023 CERN for the benefit of the ATLAS collaboration

############################################################
# AFP
############################################################

############################################################
# Reference
############################################################

reference AFPOfficialReference {
	 location = /eos/atlas/atlascerngroupdisk/data-dqm/references/,root://eosatlas.cern.ch//eos/atlas/atlascerngroupdisk/data-dqm/references/
	 file = AFPOutput62-337176-500k.root
	 path = run_337176
	 name = same_name
}

############################################################
# Output
############################################################

output top_level {
	output AFP {
		output SiT {
			output BCID_Mask {
			}
			output Aux {
			}
			output pixelColRow2D {
				output ${station} {
				}
			}
			output PixelColIDChip {
				output ${station} {
				}
			}
			output PixelRowIDChip {
				output ${station} {
				}
			}
			output SiTimeOverThreshold {
				output nearAside {
				}
				output farAside {
				}
				output nearCside {
				}
				output farCside {
				}
			}
			output HitsCounterPlanes {
			}
			output HitsPerPlanes {
			}
			output HitsPerPlanesVsLb {
			}
			output Cluster {
				output ${station} {
				}
			}
			output ClustersPerPlanesPP {
			}
			output ClusterToT {
			}
			output Track {
			}
			output TracksVsLb {
				output ${fme} {
				}
			}
			output StationEvents {
			}
			output PP {
				output HitOccupancy {
					output PlaneOccupancy {
					}
					output Occupancy_vs_lb {
					}
				}
				output TrackFME {
					output ${fme} {
					}
					output Middle {
					}
				}
				output ClustersFME {
				}
			}
		}
		output ToF {
			output HitBarvsTrain {
			}
			output HitsPerBarsInTrain {
				output ${station} {
				}
			}
			output StationHits {
			}
			output ToFHitsVsLb {
				output side${side} {
					output All {
					}
					output ${fme} {
					}
				}
			}
			output Events {
			}
			output PP {
				output HitsByEvents {
				}
				output ToFHitsVsLbByEvents {
					output side${side} {
						output All {
						}
						output ${fme} {
						}
					}
				}
			}
		}
		output ToFSiTCorr {
		}
	}
}

############################################################
# Histogram Assessments
############################################################

dir AFP {
	algorithm = Histogram_Not_Empty
	display = Draw=HIST
	dir SiT {
		output = AFP/SiT
		hist nSiHits_vs_lb {
		}
		hist muPerBX_vs_lb {
		}
		dir BCID_Mask {
			output = AFP/SiT/BCID_Mask
			hist bcid(All|Front|Middle|End) {
				regex = 1
			}
		}
		dir Aux {
			output = AFP/SiT/Aux
			hist [Nn]umberOfEventsPerLumiblock(|Front|Middle|End) {
				regex = 1
			}
			hist numberOfHitsPerStation {
			}
			hist NumberOfHitsPerLumiblock {
			}
			hist eventsPerStation {
			}
			hist clustersInPlanes {
			}
		}
		dir pixelColRow2D {
			display = LogZ,Draw=COLZ
			dir (?P<station>(far|near)[AC]side) {
				regex = 1
				output = AFP/SiT/pixelColRow2D/${station}
				hist pixelColIDChip_vs_pixelRowIDChip_(?P=station)_P[0123] {
				}
			}
		}
		dir PixelColIDChip {
			algorithm = AFPBinContentComp
			dir (?P<station>(far|near)[AC]side) {
				regex = 1
				output = AFP/SiT/PixelColIDChip/${station}
				hist pixelColIDChip_(far|near)[AC]side_P[0123] {
				}
			}
		}
		dir PixelRowIDChip {
			algorithm = AFPBinContentComp
			dir (?P<station>(far|near)[AC]side) {
				regex = 1
				output = AFP/SiT/PixelRowIDChip/${station}
				hist pixelRowIDChip_(far|near)[AC]side_P[0123] {
				}
			}
		}
		dir SiTimeOverThreshold {
			dir nearAside {
				output = AFP/SiT/SiTimeOverThreshold/nearAside
				hist timeOverThreshold_nearAside_P0 {
					algorithm = AFPBinContentComp/ToT/NearA0
				}
				hist timeOverThreshold_nearAside_P1 {
					algorithm = AFPBinContentComp/ToT/NearA1
				}
				hist timeOverThreshold_nearAside_P2 {
					algorithm = AFPBinContentComp/ToT/NearA2
				}
				hist timeOverThreshold_nearAside_P3 {
					algorithm = AFPBinContentComp/ToT/NearA3
				}
			}
			dir farAside {
				output = AFP/SiT/SiTimeOverThreshold/farAside
				hist timeOverThreshold_farAside_P0 {
					algorithm = AFPBinContentComp/ToT/FarA0
				}
				hist timeOverThreshold_farAside_P1 {
					algorithm = AFPBinContentComp/ToT/FarA1
				}
				hist timeOverThreshold_farAside_P2 {
					algorithm = AFPBinContentComp/ToT/FarA2
				}
				hist timeOverThreshold_farAside_P3 {
					algorithm = AFPBinContentComp/ToT/FarA3
				}
			}
			dir nearCside {
				output = AFP/SiT/SiTimeOverThreshold/nearCside
				hist timeOverThreshold_nearCside_P0 {
					algorithm = AFPBinContentComp/ToT/NearC0
				}
				hist timeOverThreshold_nearCside_P1 {
					algorithm = AFPBinContentComp/ToT/NearC1
				}
				hist timeOverThreshold_nearCside_P2 {
					algorithm = AFPBinContentComp/ToT/NearC2
				}
				hist timeOverThreshold_nearCside_P3 {
					algorithm = AFPBinContentComp/ToT/NearC3
				}
			}
			dir farCside {
				output = AFP/SiT/SiTimeOverThreshold/farCside
				hist timeOverThreshold_farCside_P0 {
					algorithm = AFPBinContentComp/ToT/FarC0
				}
				hist timeOverThreshold_farCside_P1 {
					algorithm = AFPBinContentComp/ToT/FarC1
				}
				hist timeOverThreshold_farCside_P2 {
					algorithm = AFPBinContentComp/ToT/FarC2
				}
				hist timeOverThreshold_farCside_P3 {
					algorithm = AFPBinContentComp/ToT/FarC3
				}
			}
		}
		dir HitsCounterPlanes {
			output = AFP/SiT/HitsCounterPlanes
			hist hitsCounterPlanesTProfile_vs_lb_(far|near)[AC]side_P[0123] {
				regex = 1
			}
		}
		dir HitsPerPlanes {
			output = AFP/SiT/HitsPerPlanes
			hist planeHitsAll {
			}
			hist planeHitsAllMU {
			}
			hist planeHits_(far|near)[AC]side {
				regex = 1
				algorithm = AFPBinContentComp
			}
		}
		dir HitsPerPlanesVsLb {
			output = AFP/SiT/HitsPerPlanesVsLb
			display = Draw=COLZ
			hist lbHitsPerPlanes_(far|near)[AC]side_P[0123] {
				regex = 1
			}
		}
		dir ClustersPerPlanesPP {
			output = AFP/SiT/ClustersPerPlanesPP
			display = Draw=COLZ
			hist lbClustersPerPlanes_(far|near)[AC]side_P[0123] {
				regex = 1
			}
		}
		dir Cluster {
			display = LogZ,Draw=COLZ
			dir (?P<station>(far|near)[AC]side) {
				regex = 1
				output = AFP/SiT/Cluster/${station}
				hist clusterX_vs_clusterY_(?P=station)_P[0123] {
				}
			}
		}
		dir ClusterToT {
			output = AFP/SiT/ClusterToT
			hist clusterToT_(far|near)[AC]side_P[0123] {
				regex = 1
			}
		}
		dir Track {
			output = AFP/SiT/Track
			display = LogZ,Draw=COLZ
			hist trackX_vs_trackY_(far|near)[AC]side {
				regex = 1
			}
		}
		dir TracksVsLb {
			output = AFP/SiT/TracksVsLb
			dir (?P<fme>Front|Middle|End) {
				regex = 1
				output = AFP/SiT/TracksVsLb/${fme}
				hist lbTracks(?P=fme)_(far|near)[AC]side {
				}
			}
			hist lbTracksAll_(far|near)[AC]side {
				regex = 1
			}
		}
		dir StationEvents {
			output = AFP/SiT/StationEvents
			hist lbEventsStationsAll {
			}
			hist lbEventsStations_(far|near)[AC]side {
				regex = 1
			}
		}
		dir PP {
			dir HitOccupancy {
				dir PlaneOccupancy {
					output = AFP/SiT/PP/HitOccupancy/PlaneOccupancy
					hist OccupancyAllMU {
					}
				}
				dir Occupancy_vs_lb {
					output = AFP/SiT/PP/HitOccupancy/Occupancy_vs_lb
					hist Occupancy_vs_lb_farAside_P0 {
						algorithm = AFPBinsOutOfRange/Occupancy/FarA0
					}
					hist Occupancy_vs_lb_farAside_P1 {
						algorithm = AFPBinsOutOfRange/Occupancy/FarA1
					}
					hist Occupancy_vs_lb_farAside_P2 {
						algorithm = AFPBinsOutOfRange/Occupancy/FarA2
					}
					hist Occupancy_vs_lb_farAside_P3 {
						algorithm = AFPBinsOutOfRange/Occupancy/FarA3
					}
					hist Occupancy_vs_lb_nearAside_P0 {
						algorithm = AFPBinsOutOfRange/Occupancy/NearA0
					}
					hist Occupancy_vs_lb_nearAside_P1 {
						algorithm = AFPBinsOutOfRange/Occupancy/NearA1
					}
					hist Occupancy_vs_lb_nearAside_P2 {
						algorithm = AFPBinsOutOfRange/Occupancy/NearA2
					}
					hist Occupancy_vs_lb_nearAside_P3 {
						algorithm = AFPBinsOutOfRange/Occupancy/NearA3
					}
					hist Occupancy_vs_lb_farCside_P0 {
						algorithm = AFPBinsOutOfRange/Occupancy/FarC0
					}
					hist Occupancy_vs_lb_farCside_P1 {
						algorithm = AFPBinsOutOfRange/Occupancy/FarC1
					}
					hist Occupancy_vs_lb_farCside_P2 {
						algorithm = AFPBinsOutOfRange/Occupancy/FarC2
					}
					hist Occupancy_vs_lb_farCside_P3 {
						algorithm = AFPBinsOutOfRange/Occupancy/FarC3
					}
					hist Occupancy_vs_lb_nearCside_P0 {
						algorithm = AFPBinsOutOfRange/Occupancy/NearC0
					}
					hist Occupancy_vs_lb_nearCside_P1 {
						algorithm = AFPBinsOutOfRange/Occupancy/NearC1
					}
					hist Occupancy_vs_lb_nearCside_P2 {
						algorithm = AFPBinsOutOfRange/Occupancy/NearC2
					}
					hist Occupancy_vs_lb_nearCside_P3 {
						algorithm = AFPBinsOutOfRange/Occupancy/NearC3
					}
				}
			}
			dir TrackFME {
				output = AFP/SiT/PP/TrackFME
				hist lbTracksAll_vs_lb_(far|near)[AC]side {
					regex = 1
				}
				dir (?P<fme>Front|End) {
					regex = 1
					output = AFP/SiT/PP/TrackFME/${fme}
					hist lbTracks(?P=fme)_vs_lb_nearCside {
						algorithm = AFPLBsOutOfRange/TrackFE/NearC
					}
					hist lbTracks(?P=fme)_vs_lb_nearAside {
						algorithm = AFPLBsOutOfRange/TrackFE/NearA
					}
					hist lbTracks(?P=fme)_vs_lb_farCside {
						algorithm = AFPLBsOutOfRange/TrackFE/FarC
					}
					hist lbTracks(?P=fme)_vs_lb_farAside {
						algorithm = AFPLBsOutOfRange/TrackFE/FarA
					}
				}
				dir Middle {
					output = AFP/SiT/PP/TrackFME/Middle
					hist lbTracksMiddle_vs_lb_(far|near)[AC]side {
						regex = 1
					}
				}
			}
			dir ClustersFME {
				output = AFP/SiT/PP/ClustersFME
				hist nearAside_P0 {
					algorithm = AFPLBsOutOfRange/ClusterFME/NearA0
				}
				hist nearAside_P1 {
					algorithm = AFPLBsOutOfRange/ClusterFME/NearA1
				}
				hist nearAside_P2 {
					algorithm = AFPLBsOutOfRange/ClusterFME/NearA2
				}
				hist nearAside_P3 {
					algorithm = AFPLBsOutOfRange/ClusterFME/NearA3
				}
				hist nearCside_P0 {
					algorithm = AFPLBsOutOfRange/ClusterFME/NearC0
				}
				hist nearCside_P1 {
					algorithm = AFPLBsOutOfRange/ClusterFME/NearC1
				}
				hist nearCside_P2 {
					algorithm = AFPLBsOutOfRange/ClusterFME/NearC2
				}
				hist nearCside_P3 {
					algorithm = AFPLBsOutOfRange/ClusterFME/NearC3
				}
				hist farAside_P0 {
					algorithm = AFPLBsOutOfRange/ClusterFME/FarA0
				}
				hist farAside_P1 {
					algorithm = AFPLBsOutOfRange/ClusterFME/FarA1
				}
				hist farAside_P2 {
					algorithm = AFPLBsOutOfRange/ClusterFME/FarA2
				}
				hist farAside_P3 {
					algorithm = AFPLBsOutOfRange/ClusterFME/FarA3
				}
				hist farCside_P0 {
					algorithm = AFPLBsOutOfRange/ClusterFME/FarC0
				}
				hist farCside_P1 {
					algorithm = AFPLBsOutOfRange/ClusterFME/FarC1
				}
				hist farCside_P2 {
					algorithm = AFPLBsOutOfRange/ClusterFME/FarC2
				}
				hist farCside_P3 {
					algorithm = AFPLBsOutOfRange/ClusterFME/FarC3
				}
			}
		}
	}
	dir ToF {
		output = AFP/ToF
		hist numberOfHit_S[03] {
			regex = 1
		}
		dir HitBarvsTrain {
			output = AFP/ToF/HitBarvsTrain
			display = Draw=COLZ
			hist trainID_vs_barInTrainID_far[AC]side {
				regex = 1
			}
		}
		dir HitsPerBarsInTrain {
			output = AFP/ToF/HitsPerBarsInTrain
			hist barInTrainAll[AC] {
				regex = 1
			}
			dir (?P<station>far[AC]side) {
				regex = 1
				output = AFP/ToF/HitsPerBarsInTrain/${station}
				algorithm = AFPBinContentComp
				display = Draw=COLZ
				hist barInTrainID[AC]_train[0123] {
				}
			}
		}
		dir StationHits {
			output = AFP/ToF/StationHits
			display = Draw=COLZ
			hist ToFHits_side[AC] {
				regex = 1
			}
		}
		dir Events {
			output = AFP/ToF/Events
			display = Draw=COLZ
			hist lb[AC]ToFEvents {
				regex = 1
			}
			hist lbAandCToFEvents {
			}
		}
		dir ToFHitsVsLb {
			dir side(?P<side>A|C) {
				regex = 1
				output = AFP/ToF/ToFHitsVsLb/side${side}
				dir All {
					output = AFP/ToF/ToFHitsVsLb/side${side}/All
					hist lb(?P=side)ToF_T[0123] {
					}
				}
				dir (?P<fme>Front|Middle|End) {
					output = AFP/ToF/ToFHitsVsLb/side${side}/${fme}
					hist lb(?P=side)ToF_T[0123]_(?P=fme) {
					}
				}
				hist lb(?P=side)ToF_T[0123]_[ABCD] {
				}
			}
		}
		dir PP {
			dir HitsByEvents {
				output = AFP/ToF/PP/HitsByEvents
				hist HitsByEvents_side[AC] {
					regex = 1
				}
			}
			dir ToFHitsVsLbByEvents {
				dir side(?P<side>A|C) {
					regex = 1
					output = AFP/ToF/PP/ToFHitsVsLbByEvents/side${side}
					dir All {
						output = AFP/ToF/PP/ToFHitsVsLbByEvents/side${side}/All
						hist lb(?P=side)ToF_T[0123]_Vs_Events {
						}
					}
					dir (?P<fme>Front|Middle|End) {
						output = AFP/ToF/PP/ToFHitsVsLbByEvents/side${side}/${fme}
						hist lb(?P=side)ToF_T[0123]_Vs_Events_(?P=fme) {
						}
					}
					hist lb(?P=side)ToF_T[0123]_[ABCD]_Vs_Events {
					}
				}
			}
		}
	}
	dir ToFSiTCorr {
		output = AFP/ToFSiTCorr
		display = Draw=COLZ
		hist ToFSiTCorr[XY][AC] {
			regex = 1
		}
		hist ToFSiTCorrTight[XY][AC] {
			regex = 1
		}
		hist ToFSiTCorrTrainHits[XY][AC] {
			regex = 1
		}
		hist ToFSiTNumHits[AC] {
			regex = 1
		}
	}
}

############################################################
# Algorithms
############################################################

algorithm AFPBinContentComp {
	libname = libdqm_algorithms.so
	name = BinContentComp
	thresholds = AFPBinContentCompThreshold
	NSigma = 3
	reference = AFPOfficialReference
	publish = 1
	NormRef = 1
	IncludeRefError = 1
	algorithm ToT {
		algorithm FarA0 {
			NSigma = 435.22592102144
		}
		algorithm FarA1 {
			NSigma = 507.930957667639
		}
		algorithm FarA2 {
			NSigma = 519.895877176801
		}
		algorithm FarA3 {
			NSigma = 500
		}
		algorithm NearA0 {
			NSigma = 290.712331067686
		}
		algorithm NearA1 {
			NSigma = 316.684303913076
		}
		algorithm NearA2 {
			NSigma = 358.752647078107
		}
		algorithm NearA3 {
			NSigma = 313.697123770129
		}
		algorithm FarC0 {
			NSigma = 288.696902600624
		}
		algorithm FarC1 {
			NSigma = 416.637879941265
		}
		algorithm FarC2 {
			NSigma = 372.472028534828
		}
		algorithm FarC3 {
			NSigma = 296.824017348296
		}
		algorithm NearC0 {
			NSigma = 347.123372535644
		}
		algorithm NearC1 {
			NSigma = 344.280843343399
		}
		algorithm NearC2 {
			NSigma = 370.000144656172
		}
		algorithm NearC3 {
			NSigma = 390.892193752344
		}
	}
}

algorithm AFPBinsOutOfRange {
	libname = libdqm_algorithms.so
	name = BinsOutOfRange
	ignoreval = 0
	thresholds = AFPBinsOutOfRangeThreshold
	MaxPublish = 50
	algorithm Occupancy {
		algorithm FarA0 {
			RANGE_D = 0.064623201730579
			RANGE_U = 0.082074196601526
		}
		algorithm FarA1 {
			RANGE_D = 0.069210760996736
			RANGE_U = 0.091298127631908
		}
		algorithm FarA2 {
			RANGE_D = 0.011390473243954
			RANGE_U = 0.056488146158625
		}
		algorithm FarA3 {
			RANGE_D = 0
			RANGE_U = 1
		}
		algorithm NearA0 {
			RANGE_D = 0.036528025554269
			RANGE_U = 0.047734777429346
		}
		algorithm NearA1 {
			RANGE_D = 0.042102300740159
			RANGE_U = 0.056155446119697
		}
		algorithm NearA2 {
			RANGE_D = 0.050365976880481
			RANGE_U = 0.065725886148604
		}
		algorithm NearA3 {
			RANGE_D = 0.067090599418169
			RANGE_U = 0.084797032284963
		}
		algorithm FarC0 {
			RANGE_D = 0.056301586864145
			RANGE_U = 0.077068084699265
		}
		algorithm FarC1 {
			RANGE_D = 0.016128092507202
			RANGE_U = 0.029998648464199
		}
		algorithm FarC2 {
			RANGE_D = 0.071307409102957
			RANGE_U = 0.096288712524791
		}
		algorithm FarC3 {
			RANGE_D = 0.089866811133015
			RANGE_U = 0.12027871559395
		}
		algorithm NearC0 {
			RANGE_D = 0.032829272673564
			RANGE_U = 0.044755449187577
		}
		algorithm NearC1 {
			RANGE_D = 0.040389786593796
			RANGE_U = 0.052695470105802
		}
		algorithm NearC2 {
			RANGE_D = 0.048510322299615
			RANGE_U = 0.06161289457124
		}
		algorithm NearC3 {
			RANGE_D = 0.060693038117471
			RANGE_U = 0.077891768660102
		}
	}
}

algorithm AFPLBsOutOfRange {
	libname = libdqm_algorithms.so
	name = BinsOutOfRange
	ignoreval = 0
	MaxPublish = 50
	algorithm ClusterFME {
		thresholds = AFPLBsOutOfRangeClusterFME
		algorithm FarA0 {
			RANGE_D = 0.033589416057268
			RANGE_U = 0.043025369931549
		}
		algorithm FarA1 {
			RANGE_D = 0.037341934754166
			RANGE_U = 0.049298875780506
		}
		algorithm FarA2 {
			RANGE_D = 0.010015546936473
			RANGE_U = 0.042046208389909
		}
		algorithm FarA3 {
			RANGE_D = 0.018828405350083
			RANGE_U = 0.025700544163024
		}
		algorithm NearA0 {
			RANGE_D = 0.0189895105317533
			RANGE_U = 0.0253004399180363
		}
		algorithm NearA1 {
			RANGE_D = 0.021099213322412
			RANGE_U = 0.029418032108873
		}
		algorithm NearA2 {
			RANGE_D = 0.024561680285696
			RANGE_U = 0.032324944314004
		}
		algorithm NearA3 {
			RANGE_D = 0.03149254498641
			RANGE_U = 0.040965050050775
		}
		algorithm FarC0 {
			RANGE_D = 0.030672301225943
			RANGE_U = 0.041127700786991
		}
		algorithm FarC1 {
			RANGE_D = 0.013659975702608
			RANGE_U = 0.024612416983401
		}
		algorithm FarC2 {
			RANGE_D = 0.038302698826789
			RANGE_U = 0.050626634219323
		}
		algorithm FarC3 {
			RANGE_D = 0.044512872596221
			RANGE_U = 0.058214184332748
		}
		algorithm NearC0 {
			RANGE_D = 0.018828405350083
			RANGE_U = 0.025700544163024
		}
		algorithm NearC1 {
			RANGE_D = 0.021471041551783
			RANGE_U = 0.028230733238655
		}
		algorithm NearC2 {
			RANGE_D = 0.024082844613334
			RANGE_U = 0.031348742757091
		}
		algorithm NearC3 {
			RANGE_D = 0.030101860458927
			RANGE_U = 0.038875395074369
		}
	}
	algorithm TrackFE {
		thresholds = AFPLBsOutOfRangeTrackFE
		algorithm FarA {
			RANGE_D = 0.001809334
		}
		algorithm NearA {
			RANGE_D = 0.014799335
		}
		algorithm FarC {
			RANGE_D = 0.020306089
		}
		algorithm NearC {
			RANGE_D = 0.014350775
		}
	}
}

############################################################
# Thresholds
############################################################

thresholds AFPBinContentCompThreshold {
	limits NBins {
		warning = 1
		error = 2
	}
}

thresholds AFPBinsOutOfRangeThreshold {
	limits NbadBins {
		warning = 3
		error = 25
	}
}

thresholds AFPLBsOutOfRangeClusterFME {
	limits NbadBins {
		warning = 16
		error = 25
	}
}

thresholds AFPLBsOutOfRangeTrackFE {
	limits NbadBins {
		warning = 31
		error = 35
	}
}
